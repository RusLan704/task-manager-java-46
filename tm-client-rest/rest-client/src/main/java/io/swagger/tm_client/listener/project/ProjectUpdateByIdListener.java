package io.swagger.tm_client.listener.project;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import io.swagger.tm_client.event.ConsoleEvent;
import io.swagger.tm_client.listener.AbstractListener;
import io.swagger.tm_client.util.TerminalUtil;

@Component
public final class ProjectUpdateByIdListener extends AbstractListener {

    @NotNull
    @Autowired
    private DefaultApi api;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by id.";
    }

    @EventListener(condition = "@projectUpdateByIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) throws ApiException {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = api.findById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectDTO newProject = new ProjectDTO();
        newProject.setId(id);
        newProject.setName(name);
        newProject.setDescription(description);
        @NotNull final ProjectDTO projectUpdated = api.updateProjectById(newProject);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}