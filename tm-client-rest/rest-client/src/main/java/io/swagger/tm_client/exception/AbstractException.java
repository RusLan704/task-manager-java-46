package io.swagger.tm_client.exception;

import org.jetbrains.annotations.NotNull;

public class AbstractException extends RuntimeException {

    @NotNull
    public AbstractException() {
    }

    @NotNull
    public AbstractException(@NotNull Throwable cause) {
        super(cause);
    }

    @NotNull
    public AbstractException(@NotNull String message) {
        super(message);
    }

}