package io.swagger.tm_client.listener.project;

import io.swagger.client.ApiException;
import io.swagger.client.api.DefaultApi;
import io.swagger.client.model.ProjectDTO;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import io.swagger.tm_client.event.ConsoleEvent;
import io.swagger.tm_client.listener.AbstractListener;

import java.util.List;

@Component
public final class ProjectListListener extends AbstractListener {

    @NotNull
    @Autowired
    private DefaultApi api;

    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list.";
    }

    @EventListener(condition = "@projectListListener.name() == #event.name")
    public void handler(final ConsoleEvent event) throws ApiException {
        System.out.println("[LIST PROJECT]");
        @NotNull final List<ProjectDTO> projects = api.findAll();
        int index = 1;
        for (@NotNull final ProjectDTO project : projects) {
            System.out.println(index + ". " + project.getId() + ": " + project.getName());
            index++;
        }
        System.out.println("[OK]");
    }

}