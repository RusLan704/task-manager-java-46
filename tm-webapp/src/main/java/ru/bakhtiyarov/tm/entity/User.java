package ru.bakhtiyarov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.enumeration.RoleType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "tm_user")
public final class User extends AbstractEntity {

    public static final long serialVersionUID = 1L;

    @NotNull
    @Column(nullable = false)
    private String login = "";

    @NotNull
    @Column(unique = true, nullable = false)
    private String passwordHash = "";

    @Column
    @NotNull
    private String email = "";

    @Column
    @NotNull
    private String firstName = "";

    @Column
    @NotNull
    private String lastName = "";

    @Column
    @NotNull
    private String middleName = "";

    @Column(nullable = false)
    private Boolean locked = false;

    @Column
    @NotNull
    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    private List<Role> roles = new ArrayList<>();

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    public User(@NotNull String login, @NotNull String passwordHash) {
        this.login = login;
        this.passwordHash = passwordHash;
    }

}
