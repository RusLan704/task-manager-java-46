package ru.bakhtiyarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.core.userdetails.User;
import ru.bakhtiyarov.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(@Nullable final String userId, @Nullable Task task, @Nullable String projectId);

    @NotNull
    Task updateTaskById(@Nullable final String userId, @Nullable final Task task);

    @NotNull
    List<Task> findAll(@Nullable final String userId);

    @NotNull
    List<Task> findAll();

    @NotNull
    List<Task> findAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId);

    @Nullable
    Task findOneById(@Nullable final String userId, @Nullable String id);

    @Nullable
    Task findOneByIndex(@Nullable final String userId, @Nullable Integer index);

    @Nullable
    Task findById(@Nullable final String id);

    @Nullable
    Task findOneByName(@Nullable final String userId, @Nullable String name);

    void removeAll(@Nullable final String userId);

    void removeOneByIndex(@Nullable final String userId, @Nullable Integer index);

    void removeOneById(@Nullable final String userId, @Nullable String id);

    void removeOneById(@Nullable String id);

    void removeOneByName(@Nullable final String userId, @Nullable String name);

    void removeAllByUserIdAndProjectId(@Nullable final String userId, @Nullable final String projectId);

    void removeAll();

}
