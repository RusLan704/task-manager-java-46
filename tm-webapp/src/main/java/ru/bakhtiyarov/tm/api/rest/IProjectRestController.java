package ru.bakhtiyarov.tm.api.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.bakhtiyarov.tm.dto.ProjectDTO;

import java.util.List;

@RequestMapping(value = "/api/rest/project")
public interface IProjectRestController {

    @Nullable
    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_VALUE)
    ProjectDTO create(
            @RequestBody @Nullable ProjectDTO projectDTO
    );

    @NotNull
    @GetMapping(value = "/findAll", produces = MediaType.APPLICATION_JSON_VALUE)
    List<ProjectDTO> findAll();

    @GetMapping(value = "/findById/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ProjectDTO findById(
            @PathVariable("id") @Nullable String id
    );

    @PutMapping(value = "/updateById", produces = MediaType.APPLICATION_JSON_VALUE)
    ProjectDTO updateProjectById(
            @RequestBody @Nullable ProjectDTO projectDTO
    );

    @DeleteMapping(value = "/removeById/{id}")
    void removeOneById(
            @PathVariable("id") @Nullable String id
    );

}
