package ru.bakhtiyarov.tm.endpoint.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.bakhtiyarov.tm.api.rest.IAuthRestEndpoint;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.dto.UserDTO;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.service.converter.UserConverter;


@RestController
@RequestMapping("/api/rest/authentication")
public class AuthRestEndpoint implements IAuthRestEndpoint {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    @Autowired
    private UserConverter userConverter;

    @Override
    @GetMapping(value = "/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    ) {
        try {
            final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
            final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return true;
        } catch (final Exception e) {
            return false;
        }
    }

    @Override
    @GetMapping(value = "/profile", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDTO profile() {
        final SecurityContext securityContext = SecurityContextHolder.getContext();
        final Authentication authentication = securityContext.getAuthentication();
        final String username = authentication.getName();
        final User user = userService.findByLogin(username);
        return userConverter.toDTO(user);
    }

    @Override
    @GetMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

}