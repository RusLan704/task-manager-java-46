package ru.bakhtiyarov.tm.endpoint.soap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.bakhtiyarov.tm.api.service.ITaskService;
import ru.bakhtiyarov.tm.api.service.converter.ITaskConverter;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

//http://localhost:8080/ws/TaskEndpoint?wsdl

@Component
@WebService
public class TaskSoapEndpoint {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final ITaskConverter taskConverter;

    @NotNull
    @Autowired
    @SneakyThrows
    public TaskSoapEndpoint(
            @NotNull final ITaskService taskService,
            @NotNull final  ITaskConverter taskConverter
    ) {
        this.taskService = taskService;
        this.taskConverter = taskConverter;
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public Task createTask(
            @WebParam(name = "taskDTO", partName = "taskDTO") @Nullable final TaskDTO taskDTO
    ) {
        return taskService.create(
                UserUtil.getAuthUserId(),
                taskConverter.toEntity(taskDTO),
                taskDTO.getProjectId()
        );
    }

    @NotNull
    @WebMethod
    @SneakyThrows
    public List<TaskDTO> findAllTasks() {
        @NotNull final List<Task> tasks = taskService.findAll(UserUtil.getAuthUserId());
        return tasks
                .stream()
                .map(taskConverter::toDTO)
                .collect(Collectors.toList());
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public TaskDTO findTaskById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        final @Nullable Task task = taskService.findOneById(UserUtil.getAuthUserId(), id);
        return taskConverter.toDTO(task);
    }

    @Nullable
    @WebMethod
    @SneakyThrows
    public TaskDTO updateTaskById(
            @WebParam(name = "taskDTO", partName = "taskDTO") @Nullable final TaskDTO taskDTO
    ) {
        @Nullable final Task task = taskService.updateTaskById(
                UserUtil.getAuthUserId(),
                taskConverter.toEntity(taskDTO)
        );
        return taskConverter.toDTO(task);
    }

    @WebMethod
    @SneakyThrows
    public void removeTaskOneById(
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        taskService.removeOneById(UserUtil.getAuthUserId(), id);
    }

}