package ru.bakhtiyarov.tm.controller.web;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.bakhtiyarov.tm.Application;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.RoleType;
import ru.bakhtiyarov.tm.repository.IProjectRepository;
import ru.bakhtiyarov.tm.repository.ITaskRepository;
import ru.bakhtiyarov.tm.repository.IUserRepository;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class TaskControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IUserService userService;

    @NotNull
    private Task task;

    @NotNull
    private Project project;

    @NotNull
    private final String baseUrl = "/tasks";

    @Before
    @Transactional
    public void initData() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

        @Nullable final User user = userService.create("login", "pass", RoleType.ADMIN);
        project = new Project("name", "description");
        project.setUser(user);
        projectRepository.save(project);
        task = new Task("name", "description");
        task.setUser(user);
        task.setProject(project);
        taskRepository.save(task);

        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("login", "pass");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    @Transactional
    public void deleteData() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void testList() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/list"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("task/task-list"));
    }

    @Test
    public void testCreateGet() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/create"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("task/task-create"));
    }

    @Test
    public void testCreatePost() throws Exception {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("test");
        taskDTO.setDescription("test");
        taskDTO.setProjectId(project.getId());
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/create").flashAttr("task", taskDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks/list"));
    }

    @Test
    public void testRemove() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/remove/{id}", task.getId()))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks/list"));
    }

    @Test
    public void testGetUpdate() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .get(baseUrl + "/update/{id}", task.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("task/task-update"));
    }

    @Test
    public void testPostUpdate() throws Exception {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("test");
        taskDTO.setDescription("test");
        this.mockMvc.perform(MockMvcRequestBuilders
                .post(baseUrl + "/update/{id}", task.getId()).flashAttr("task", taskDTO))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/tasks/list"));
    }

}
