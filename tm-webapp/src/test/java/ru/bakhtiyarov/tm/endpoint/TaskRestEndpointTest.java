package ru.bakhtiyarov.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import ru.bakhtiyarov.tm.Application;
import ru.bakhtiyarov.tm.api.service.IUserService;
import ru.bakhtiyarov.tm.dto.TaskDTO;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Task;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.RoleType;
import ru.bakhtiyarov.tm.repository.IProjectRepository;
import ru.bakhtiyarov.tm.repository.ITaskRepository;
import ru.bakhtiyarov.tm.repository.IUserRepository;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Application.class)
public class TaskRestEndpointTest {

    @NotNull
    private final String baseUrl = "/api/rest/task";

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private ITaskRepository taskRepository;

    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IUserService userService;

    private Task task;

    private Project project;

    private User user;

    @Before
    @Transactional
    public void initData() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

        user = userService.create("login", "pass", RoleType.ADMIN);
        project = new Project("name", "description");
        project.setUser(user);
        projectRepository.save(project);
        task = new Task("name", "description");
        task.setUser(user);
        task.setProject(project);
        taskRepository.save(task);

        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("login", "pass");
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    @Transactional
    public void deleteData() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
        userRepository.deleteAll();
    }

    @Test
    public void createTest() throws Exception {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setProjectId(project.getId());

        final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        final ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        final String requestJson = ow.writeValueAsString(taskDTO);

        this.mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .post(baseUrl + "/create")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(requestJson)
                )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").value(task.getId()))
                .andExpect(jsonPath("$.name").value(task.getName()))
                .andExpect(jsonPath("$.description").value(task.getDescription()))
                .andExpect(jsonPath("$.projectId").value(project.getId()));
    }

    @Test
    public void findAllTest() throws Exception {
        this.mockMvc.perform(
                MockMvcRequestBuilders
                        .get(baseUrl + "/findAll")
        )
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$..id", hasItem(task.getId())))
                .andExpect(jsonPath("$..name", hasItem(task.getName())))
                .andExpect(jsonPath("$..description", hasItem(task.getDescription())));
    }

    @Test
    public void findByIdTest() throws Exception {
        this.mockMvc.perform(
                MockMvcRequestBuilders
                        .get(baseUrl + "/findById/{id}", task.getId()
                        )
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").value(task.getId()));
    }

    @Test
    public void testUpdateTask() throws Exception {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName("new name");
        taskDTO.setDescription("new description");

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        @NotNull final ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();
        String requestJson = writer.writeValueAsString(taskDTO);

        this.mockMvc.perform(MockMvcRequestBuilders
                .put(baseUrl + "/updateById")
                .contentType(MediaType.APPLICATION_JSON).content(requestJson))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").value(task.getId()))
                .andExpect(jsonPath("$.name").value("new name"))
                .andExpect(jsonPath("$.description").value("new description"));
    }

    @Test
    public void removeByIdTest() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders
                .delete(baseUrl + "/removeById/{id}", task.getId()))
                .andDo(print())
                .andExpect(MockMvcResultMatchers.status().isOk());
        @Nullable final Task taskDeleted = taskRepository.findById(task.getId()).orElse(null);
        Assert.assertNull(taskDeleted);
    }
    
}
